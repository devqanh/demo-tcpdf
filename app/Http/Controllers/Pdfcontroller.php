<?php

namespace App\Http\Controllers;

use Mpdf\Mpdf;

class Pdfcontroller extends Controller
{
    public function index()
    {
        $mpdf = new Mpdf([
            'mode' => 'utf-8',
            'margin_header' => 0,
            'margin_footer' => 0,
            'margin_left' => 0,
            'margin_right' => 0,
        ]);
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->DefHTMLHeaderByName(
            'header',
            '<div style="margin-top: -50px; text-align: right; font-size: 10px; height: 50px; line-height: 50px; background-image: url(https://mondaycareer.com/wp-content/uploads/2020/11/background-%C4%91%E1%BA%B9p8-1024x682.jpg)">Thansohoc.com</div>'
        );

        $mpdf->DefHTMLFooterByName(
            'footer',
            '<table width="100%" style="color: #000000">
        <tr>
            <td width="33%">My document</td>
            <td width="33%" align="center">{PAGENO}/{nbpg}</td>
            <td width="33%" style="text-align: right;">{DATE j-m-Y}</td>
        </tr>
    </table>'
        );
        $this->addFullPage($mpdf, 'Cover page');
        $this->addContentPage($mpdf);
        $this->addFullPage($mpdf, 'Cover ppage 2');
        $this->addContentPage($mpdf);
        $this->addFullPage($mpdf, 'Last page');

        // Output a PDF file directly to the browser
        $mpdf->Output();
    }


    private function addContentPage($pdf, $viewName = 'welcome') {
        $pdf->AddPage(
            '','NEXT-ODD','','','','','','','','','',
            'header', 'header', 'footer', 'footer',
            1, 1, 1, 1
        );
        $view = \View::make($viewName);
        $pdf->WriteHTML($view);
    }


    private function addFullPage($pdf, $title = "Full Page") {
        $pdf->AddPage(
            '','NEXT-ODD','','','','','','','','','',
            '','','','',
            -1,-1,-1,-1
        );
        $view = \View::make('fullpage', compact('title'));
        $pdf->WriteHTML($view);
    }
}
