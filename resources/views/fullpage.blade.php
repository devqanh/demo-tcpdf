<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
    "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Document</title>
    <style>
        body {
            width: 100vw;
            height: 100vh;
            background-image: url("https://wikilaptop.com/wp-content/uploads/2021/01/1610534235_Tong-hop-100-Background-dep-nhat.jpg");
            background-size: cover;
            color: #fff;
        }
        h1 {
            padding: 100px 0;
            text-align: center;
            font-size: 40px;
        }
    </style>
    <sethtmlpageheader value="-1" show-this-page="1" />
    <sethtmlpagefooter value="-1" show-this-page="1" />
</head>
<body>
    <h1>
        {{ $title }}
    </h1>
</body>
</html>
